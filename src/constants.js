export const API_KEY = '3a2238c1f290423db74c98eb905e6f2c';

export const PLACEHOLDER_IMAGE = 'http://via.placeholder.com/500x250/000000?text=No+Image';

export const categories = [{
    key: '',
    value: 'All'
  },
  {
    key: 'business',
    value: 'Business'
  },
  {
    key: 'entertainment',
    value: 'Entertainment'
  },
  {
    key: 'general',
    value: 'General'
  },
  {
    key: 'health',
    value: 'Health'
  },
  {
    key: 'science',
    value: 'Science'
  },
  {
    key: 'sports',
    value: 'Sports'
  },
  {
    key: 'technology',
    value: 'Technology'
  }
];

// list of available sorting
export const sortBy = [{
    key: 'relevancy',
    value: 'Relevancy'
  },
  {
    key: 'popularity',
    value: 'Popularity'
  },
  {
    key: 'publishedAt',
    value: 'Newest'
  }
];

// list of available article languages
export const language = [{
    key: '',
    value: 'All'
  },
  {
    key: 'ar',
    value: 'Arabic'
  },
  {
    key: 'de',
    value: 'German'
  },
  {
    key: 'en',
    value: 'English'
  },
  {
    key: 'es',
    value: 'Spanish'
  },
  {
    key: 'fr',
    value: 'French'
  },
  {
    key: 'he',
    value: 'Hebrew'
  },
  {
    key: 'it',
    value: 'Italian'
  },
  {
    key: 'nl',
    value: 'Dutch'
  },
  {
    key: 'no',
    value: 'Norwegian'
  },
  {
    key: 'pt',
    value: 'Portuguese'
  },
  {
    key: 'ru',
    value: 'Russian'
  },
  {
    key: 'se',
    value: 'Northern Sami'
  },
  {
    key: 'ud',
    value: 'Dont Know'
  },
  {
    key: 'zh',
    value: 'Chinese'
  }
]

// list of countries supported
export const country = [{
    key: 'ae',
    value: 'United Arab Emirates'
  },
  {
    key: 'ar',
    value: 'Argentina'
  },
  {
    key: 'at',
    value: 'Austria'
  },
  {
    key: 'au',
    value: 'Australia'
  },
  {
    key: 'be',
    value: 'Belgium'
  },
  {
    key: 'bg',
    value: 'Bulgaria'
  },
  {
    key: 'br',
    value: 'Brazil'
  },
  {
    key: 'ca',
    value: 'Canada'
  },
  {
    key: 'ch',
    value: 'Switzerland'
  },
  {
    key: 'cn',
    value: 'China'
  },
  {
    key: 'co',
    value: 'Columbia'
  },
  {
    key: 'cu',
    value: 'Cuba'
  },
  {
    key: 'cz',
    value: 'Czech Republic'
  },
  {
    key: 'de',
    value: 'Germany'
  },
  {
    key: 'eg',
    value: 'Egypt'
  },
  {
    key: 'fr',
    value: 'France'
  },
  {
    key: 'gb',
    value: 'Great Britain'
  },
  {
    key: 'gr',
    value: 'Greece'
  },
  {
    key: 'hk',
    value: 'Hong Kong'
  },
  {
    key: 'hu',
    value: 'Hungary'
  },
  {
    key: 'id',
    value: 'Indonesia'
  },
  {
    key: 'ie',
    value: 'Ireland'
  },
  {
    key: 'il',
    value: 'Israel'
  },
  {
    key: 'in',
    value: 'India'
  },
  {
    key: 'it',
    value: 'Italy'
  },
  {
    key: 'jp',
    value: 'Japan'
  },
  {
    key: 'kr',
    value: 'Korea'
  },
  {
    key: 'lv',
    value: 'Latvia'
  },
  {
    key: 'ma',
    value: 'Morocco'
  },
  {
    key: 'mx',
    value: 'Mexico'
  },
  {
    key: 'my',
    value: 'Malaysia'
  },
  {
    key: 'ng',
    value: 'Nigeria'
  },
  {
    key: 'nl',
    value: 'Netherlands'
  },
  {
    key: 'no',
    value: 'Norway'
  },
  {
    key: 'nz',
    value: 'New Zealand'
  },
  {
    key: 'ph',
    value: 'Philippines'
  },
  {
    key: 'pl',
    value: 'Poland'
  },
  {
    key: 'pt',
    value: 'Portugal'
  },
  {
    key: 'ro',
    value: 'Romania'
  },
  {
    key: 'rs',
    value: 'Serbia'
  },
  {
    key: 'ru',
    value: 'Russia'
  },
  {
    key: 'sa',
    value: 'Saudi Arabia'
  },
  {
    key: 'se',
    value: 'Sweden'
  },
  {
    key: 'sg',
    value: 'Singapore'
  },
  {
    key: 'si',
    value: 'Slovenia'
  },
  {
    key: 'sk',
    value: 'Slovakia'
  },
  {
    key: 'th',
    value: 'Thailand'
  },
  {
    key: 'tr',
    value: 'Turkey'
  },
  {
    key: 'tw',
    value: 'Taiwan'
  },
  {
    key: 'ua',
    value: 'Ukraine'
  },
  {
    key: 'us',
    value: 'United States'
  },
  {
    key: 've',
    value: 'Venezuela'
  },
  {
    key: 'za',
    value: 'South Africa'
  }
]
