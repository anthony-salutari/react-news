import React, {Component} from 'react';
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button
} from 'reactstrap';

import {Link} from 'react-router-dom';

import '../article.css';
import moment from 'moment';
import * as constants from '../constants';

export default class Article extends Component {
  constructor(props) {
    super(props);

    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);

    this.state = {
      cardHover: false
    }
  }

  onMouseEnter() {
    this.setState({
      cardHover: true
    });
  }

  onMouseLeave() {
    this.setState({
      cardHover: false
    });
  }

  render() {
    var cardClass = this.state.cardHover ? 'shadow' : 'shadow-sm';
    var articleDate = moment(this.props.article.publishedAt).format('dddd MMM YYYY, h:mm a');

    if (this.props.article.urlToImage == null) {
      var image = constants.PLACEHOLDER_IMAGE;
    } else {
      var image = this.props.article.urlToImage;
    }

    return (
      <Card className={cardClass} onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
        <CardImg top src={image} />
        <CardBody>
          <CardTitle>{this.props.article.title}</CardTitle>
          <CardSubtitle className='mt-3 mb-3'>
            <Link to={'/LatestFromSource/' + this.props.article.source.id}>
              <Button outline="outline" size='sm'>{this.props.article.source.name}</Button>
            </Link>
          </CardSubtitle>
          <CardText className='text-muted'>{articleDate}</CardText>
          <CardText>{this.props.article.description}</CardText>
          <Button color='dark' href={this.props.article.url}>Read More</Button>
        </CardBody>
      </Card>
    );
  }
}
