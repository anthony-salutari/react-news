import React, {Component} from 'react';
import {
  ButtonDropdown,
  Col,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row
} from 'reactstrap';

import * as constants from '../constants';
import Article from './Article';
import NoArticlesFound from './NoArticlesFound';

const NewsAPI = require('../newsapi');
const newsapi = new NewsAPI(process.env.REACT_APP_API_KEY);

export default class SearchResults extends Component {
  constructor(props) {
    super(props);

    this.sortByDropdownToggle = this.sortByDropdownToggle.bind(this);
    this.languageDropdownToggle = this.languageDropdownToggle.bind(this);
    this.sortByDropdownItemClicked = this.sortByDropdownItemClicked.bind(this);
    this.languageDropdownItemClicked = this.languageDropdownItemClicked.bind(this);

    this.state = {
      articles: [],
      query: '',
      pageSize: 21,
      page: 1,
      sortBy: 'popularity',
      language: '',
      sortByDropdownOpen: false,
      languageDropdownOpen: false
    };
  }

  componentDidMount() {
    this.updateData();
  }

  updateData() {
    newsapi.v2.everything({
      q: this.props.match.params.query,
      pageSize: this.state.pageSize,
      page: this.state.page,
      sortBy: this.state.sortBy,
      language: this.state.language,
    }).then(response => {
      this.setState({articles: response.articles, query: this.props.match.params.query})
    });
  }

  sortByDropdownToggle() {
    this.setState(prevState => ({
      sortByDropdownOpen: !prevState.sortByDropdownOpen
    }));
  }

  languageDropdownToggle() {
    this.setState(prevState => ({
      languageDropdownOpen: !prevState.languageDropdownOpen
    }));
  }

  sortByDropdownItemClicked(key) {
    this.setState({
      sortBy: key
    }, () => {
      this.updateData();
    });
  }

  languageDropdownItemClicked(key) {
    this.setState({
      language: key
    }, () => {
      this.updateData();
    });
  }

  render() {
    if (this.state.articles === undefined || this.state.articles.length === 0) {
      return(
        <Container>
          <Row>
            <Col md='12'>
              <h1 className='mt-4'>Results for: {this.state.query}</h1>
            </Col>
            <Col md='4' className='mt-4 mx-auto'>
              <NoArticlesFound />
            </Col>
          </Row>
        </Container>
      );
    } else {
    return (<Container>
      <Row>
        <Col md='12' className='mt-4'>
          <h1>Results for: {this.state.query}</h1>
        </Col>
        <Col md='12' className='mt-4'>
          <ButtonDropdown className='mr-3' isOpen={this.state.sortByDropdownOpen} toggle={this.sortByDropdownToggle}>
            <DropdownToggle caret>
              Sort By
            </DropdownToggle>
            <DropdownMenu>
              {
                constants.sortBy.map((sort) => {
                  return <DropdownItem onClick={() => this.sortByDropdownItemClicked(sort.key)}>{sort.value}</DropdownItem>
                })
              }
            </DropdownMenu>
          </ButtonDropdown>
          <ButtonDropdown isOpen={this.state.languageDropdownOpen} toggle={this.languageDropdownToggle}>
            <DropdownToggle caret>
              Language
            </DropdownToggle>
            <DropdownMenu>
              {
                constants.language.map((language) => {
                  return <DropdownItem onClick={() => this.languageDropdownItemClicked(language.key)}>{language.value}</DropdownItem>
                })
              }
            </DropdownMenu>
          </ButtonDropdown>
        </Col>
        {
          this.state.articles.map((article) => {
            return (<Col md='4' className='mt-4'>
              <Article article={article}/>
            </Col>)
          })
        }
      </Row>
    </Container>);
  }
}
}
