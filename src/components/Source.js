import React, {Component} from 'react';
import {
  Container,
  Row,
  Col,
  Card,
  CardText,
  Button
} from 'reactstrap';

import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faStarOutline from '@fortawesome/fontawesome-free-regular/faStar';
import faStarSolid from '@fortawesome/fontawesome-free-solid/faStar';

import '../Source.css';

export default class Source extends Component {

  constructor(props) {
    super(props);

    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
    this.favouriteButtonClicked = this.favouriteButtonClicked.bind(this);

    this.state = {
      hover: false,
      favourite: false
    };
  }

  onMouseEnter() {
    this.setState({hover: true});
  }

  onMouseLeave() {
    this.setState({hover: false});
  }

  favouriteButtonClicked() {
    this.setState(prevState => ({
      favourite: !prevState.favourite
    }));
  }

  render() {
    var cardClass = this.state.hover
      ? 'shadow'
      : 'shadow-sm';
    var favourite = this.state.favourite
      ? 'favouriteButtonSelected'
      : 'favouriteButtonUnselected';
    var icon = this.state.favourite
      ? faStarSolid
      : faStarOutline;

    return (<Container onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
      <Row>
        <Col>
          <Card className={cardClass} body="body">
            <div className='mb-3'>
              <Button href={'/LatestFromSource/' + this.props.source.id}>{this.props.source.name}</Button>
              <Button color='warning' className={favourite + ' float-right'} onClick={() => this.favouriteButtonClicked()}>
                <FontAwesomeIcon icon={icon}></FontAwesomeIcon>
              </Button>
            </div>
            <CardText>{this.props.source.description}</CardText>
          </Card>
        </Col>
      </Row>
    </Container>)
  }
}
