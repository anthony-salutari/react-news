import React, {Component} from 'react';
import {Container, Row, Col} from 'reactstrap';

import Article from './Article';

const NewsAPI = require('../newsapi');
const newsapi = new NewsAPI(process.env.REACT_APP_API_KEY);

export default class LatestFromSource extends Component {
  constructor(props) {
    super(props);

    this.state = {
      articles: []
    };
  }

  componentDidMount() {
    newsapi.v2.everything({sources: this.props.match.params.sourceId}).then(response => {
      this.setState({articles: response.articles});
    });
  }

  render() {
    return (<Container>
      <Row>
        {
          this.state.articles.map((article) => {
            return (<Col md='4' className='pt-4'>
              <Article article={article}/>
            </Col>)
          })
        }
      </Row>
    </Container>);
  }
}
