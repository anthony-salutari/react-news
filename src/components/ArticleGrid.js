import React, {Component} from 'react';

import Article from './Article';
import {
  ButtonDropdown,
  Col,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row
} from 'reactstrap';

import '../articlegrid.css';

import * as constants from '../constants';

const NewsAPI = require('../newsapi');
const newsapi = new NewsAPI(process.env.REACT_APP_API_KEY);

export default class ArticleGrid extends Component {
  constructor(props) {
    super(props);

    this.countryDropdownToggle = this.countryDropdownToggle.bind(this);
    this.categoryDropdownToggle = this.categoryDropdownToggle.bind(this);
    this.countryDropdownItemClicked = this.countryDropdownItemClicked.bind(this);
    this.categoryDropdownItemClicked = this.categoryDropdownItemClicked.bind(this);

    this.state = {
      articles: [],
      country: 'ca',
      category: '',
      countryDropdownOpen: false,
      categoryDropdownOpen: false
    };
  }

  componentDidMount() {
    this.updateData();
  }

  updateData() {
    newsapi.v2.topHeadlines({
      country: this.state.country,
      category: this.state.category
    }).then(response => {
      this.setState({
        articles: response.articles
      });
    });
  }

  countryDropdownToggle() {
    this.setState(prevState => ({
      countryDropdownOpen: !prevState.countryDropdownOpen
    }));
  }

  categoryDropdownToggle() {
    this.setState(prevState => ({
      categoryDropdownOpen: !prevState.categoryDropdownOpen
    }));
  }

  countryDropdownItemClicked(key) {
    this.setState({
      country: key
    }, () => {
      this.updateData();
    });
  }

  categoryDropdownItemClicked(key) {
    this.setState({
      category: key
    }, () => {
      this.updateData();
    });
  }

  render() {
    return (
      <Container>
        <Row className='mt-4'>
          <Col md='12'>
            <ButtonDropdown className='mr-3' isOpen={this.state.countryDropdownOpen} toggle={this.countryDropdownToggle}>
              <DropdownToggle caret>
                Country
              </DropdownToggle>
              <DropdownMenu className='scrollableMenu'>
                {
                  constants.country.map((country) => {
                    return <DropdownItem onClick={() => this.countryDropdownItemClicked(country.key)}>{country.value}</DropdownItem>
                  })
                }
              </DropdownMenu>
            </ButtonDropdown>
            <ButtonDropdown isOpen={this.state.categoryDropdownOpen} toggle={this.categoryDropdownToggle}>
              <DropdownToggle caret>
                Category
              </DropdownToggle>
              <DropdownMenu>
                {
                  constants.categories.map((category) => {
                    return <DropdownItem onClick={() => this.categoryDropdownItemClicked(category.key)}>{category.value}</DropdownItem>
                  })
                }
              </DropdownMenu>
            </ButtonDropdown>
          </Col>
          {
            this.state.articles.map((article) => {
              return (
                <Col md='4' className='mt-4'>
                  <Article article={article}/>
                </Col>)
              }
            )
          }
        </Row>
      </Container>
    );
  }
}
