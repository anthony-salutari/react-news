import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Form, Input, Button} from 'reactstrap';

import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faSearch from '@fortawesome/fontawesome-free-solid/faSearch';

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '',
    };

    this.searchInputChanged = this.searchInputChanged.bind(this);
    this.submit = this.submit.bind(this);
  }

  searchInputChanged(event) {
    this.setState({query: event.target.value});
  }

  submit(event) {
    var path = '/searchresults/' + this.state.query;
    this.props.history.replace(path);
  }

  render() {
    return(
      <Form inline onSubmit={this.submit}>
        <Input type='search' placeholder='Search' className='form-control mr-2' value={this.state.query} onChange={this.searchInputChanged}/>
        <Button outline type='submit'><FontAwesomeIcon icon={faSearch}/></Button>
      </Form>
    );
  }
}

export default withRouter(SearchBar);
