import React, {Component} from 'react';
import {
  Container,
  Row,
  Col,
  ButtonGroup,
  Button,
  Card,
  CardTitle
} from 'reactstrap';
import Source from './Source';
import * as constants from '../constants';

const NewsAPI = require('../newsapi');
const newsapi = new NewsAPI(constants.API_KEY);

export default class Sources extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sources: [],
      categories: [
        {
          key: 0,
          name: 'All'
        }, {
          key: 1,
          name: 'Entertainment'
        }, {
          key: 2,
          name: 'General'
        }, {
          key: 3,
          name: 'Health'
        }, {
          key: 4,
          name: 'Science'
        }, {
          key: 5,
          name: 'Sports'
        }, {
          key: 6,
          name: 'Technology'
        }
      ]
    };
  }

  componentDidMount() {
    newsapi.v2.sources().then(response => {
      this.setState({sources: response.sources});
    });
  }

  categoryOnClick(category) {
    newsapi.v2.sources({category: category.name}).then(response => {
      this.setState({sources: response.sources})
    });
  }

  render() {
    return (<Container fluid>
      <Row>
        <Col md='4' lg='2'>
          <Card className='text-center shadow-sm mt-4' body>
            <CardTitle>Categories</CardTitle>
            <ButtonGroup vertical="vertical">
              {
                this.state.categories.map((category) => {
                  return (<Button outline="outline" className='mb-4' onClick={() => this.categoryOnClick(category)}>{category.name}</Button>)
                })
              }
            </ButtonGroup>
          </Card>
        </Col>
        <Col md='8' lg='10'>
          <Container>
            <Row>
              {
                this.state.sources.map((source) => {
                  return (<Col md='6' lg='4' className='mt-4'>
                    <Source source={source}/>
                  </Col>)
                })
              }
            </Row>
          </Container>
        </Col>
      </Row>
    </Container>);
  }
}
