import React from 'react';

export default function Footer() {
  return (<div className='bg-dark pt-3 pb-3 mt-4'>
    <footer>
      <p className='text-center text-white'>&copy; 2018 Anthony Salutari</p>
    </footer>
  </div>);
}
