import React from 'react';
import {
  Card,
  CardText
} from 'reactstrap';

export default function NoArticlesFound() {
  return(
    <Card body className='shadow-sm text-center'>
      <CardText>No articles found</CardText>
    </Card>
  );
}
