import React, {Component} from 'react';
import {
  Collapse,
  Container,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

import SearchBar from './SearchBar';

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
      <Navbar color="dark" dark expand="md">
        <Container>
          <NavbarBrand href="/">React News</NavbarBrand>
          <NavbarToggler onClick={this.toggle}/>
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav navbar className='mr-auto'>
              <NavItem>
                <NavLink href='/top-news/'>Top News</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href='/sources/'>Sources</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href='#'>Your Feed</NavLink>
              </NavItem>
            </Nav>

            <SearchBar />

          </Collapse>
        </Container>
      </Navbar>
    );
  }
}
