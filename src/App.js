import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import './App.css';

import Header from './components/Header';
import TopNews from './components/TopNews';
import Sources from './components/Sources';
import Footer from './components/Footer';
import LatestFromSource from './components/LatestFromSource';
import SearchResults from './components/SearchResults';

import ReactGA from 'react-ga';
ReactGA.initialize('UA-117101008-4');
ReactGA.pageview(window.location.pathname);

class App extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (<BrowserRouter>
      <div className='content bg-light'>
        <Header/>

        <Route exact="exact" path='/' component={TopNews}/>
        <Route path='/top-news' component={TopNews}/>
        <Route path='/sources' component={Sources}/>
        <Route path='/LatestFromSource/:sourceId' component={LatestFromSource}/>
        <Route path='/searchresults/:query' component={SearchResults}/>

        <Footer/>
      </div>
    </BrowserRouter>);
  }
}

export default App;
